\documentclass[a4paper, article, oneside, leqno]{memoir}

% Indexering från noll
\makeatletter
\def\@arabic#1{\number\numexpr#1-1\relax}
\makeatother

% Förfinar typografin, bland annat med marginalkerning.
\usepackage{microtype}

\usepackage{fontspec}
\usepackage{polyglossia}
\setdefaultlanguage{swedish}

\usepackage{datetime}
\renewcommand{\dateseparator}{-}

\usepackage[inline]{enumitem}

% Definiera färgnamn
\usepackage[hyperref, table, dvipsnames, svgnames, x11names]{xcolor}

% Matematik
\usepackage[intlimits]{mathtools}
\usepackage{amssymb}
\usepackage{mathrsfs}

% Gör hänvisningar inom dokumentet till länkar. Generera disposition.
\usepackage[bookmarksdepth=subsection]{hyperref}
\setsecnumdepth{subsection}

\title{Relativitetsanteckningar}
%\author{Författare}
%\date{\today\ (\currenttime)}

\hypersetup{
  unicode,
  pdftitle={\thetitle},
  %pdfauthor={\theauthor}
}

\begin{document}

\maketitle

\chapter{Topologi}

\section{Topologiskt rum}

Låt $M$ vara en mängd. En \emph{topologi} på $M$ är en mängd $\mathcal{O} \in \mathcal{P}(M)$ som uppfyller
\begin{enumerate}
\item Tomma mängden: $\varnothing \in \mathcal{O}$
\item Hela rummet: $M \in \mathcal{O}$
\item Ändliga skärningar: $U \in \mathcal{O} \wedge V \in \mathcal{O} \Rightarrow U \cap V \in \mathcal{O}$
\item Godtyckliga unioner: $(\forall \alpha \in I : U_\alpha \in \mathcal{O}) \Rightarrow \bigcup_{\alpha \in I} U_\alpha \in \mathcal{O}$, för en godtycklig mängd $I$.
\end{enumerate}
En mängd $U$ kallas \emph{öppen} om $U \in \mathcal{O}$. En mängd $A$ kallas \emph{sluten} om $M \setminus A \in \mathcal{O}$. Om $\mathcal{O}$ är en topologi på $M$ kallas $(M,\mathcal{O})$ ett \emph{topologiskt rum}.

\section{Underrumstopologi}

Givet ett topologiskt rum $(M, \mathcal{O})$ och en delmängd $S \subseteq M$ kan en topologi $\mathcal{O}|_S$ på $S$ ärvas enligt $\mathcal{O}|_S = \{U \cap S | U \in \mathcal{O}\}$.

\section{Kontinuerlig funktion}

Låt $(M, \mathcal{O}_M)$ och $(N, \mathcal{O}_N)$ vara topologiska rum. En funktion $f:M\rightarrow N$ kallas \emph{kontinuerlig} om den lyfter tillbaka öppna mängder i $N$ till öppna mängder i $M$, dvs om, för varje $V \in \mathcal{O}_N$, $\{m \in M | f(m) \in V\} \in \mathcal{O}_M$.

\section{Homeomorfi}

En \emph{homeomorfi} mellan två topologiska rum $M$ och $N$ är en kontinuerlig bijektion $h : M \rightarrow N$ med kontinuerlig invers.

\chapter{Topologiska mångfalder}

\section{Topologisk mångfald}

En \emph{topologisk mångfald} $(M, \mathcal{O}, \mathcal{A})$ är ett topologiskt rum $(M, \mathcal{O})$ utrustat med en atlas $\mathcal{A}$. En \emph{atlas} är en mängd av kartor, där varje karta $(U_\alpha, x_\alpha)$ består av en öppen domän $U_\alpha \subseteq M$ och  en homeomorfi $x_\alpha : U_\alpha \rightarrow E_\alpha \subseteq \mathbb{R}^d$, mellan kartdomänen och en öppen delmängd $E_\alpha$ i $\mathbb{R}^d$. Tillsammans täcker kartorna $M$, det vill säga $\bigcup_\alpha U_\alpha = M$.

Notera eftersom alla kartprojektioner ($x$ och $y$) är kontinuerliga och har kontinuerliga inverser gäller samma sak för alla kooridnatbytesfunktioner ($y \circ x^{-1}$).

\section{Kontinuerlig kurva i en topologisk mångfald}

En kurva $\gamma : \mathbb{R} \rightarrow M$ i en topologisk mångfald $M$ betraktas som kontinuerlig om varje sammansättning $x \circ \gamma : \mathbb{R} \rightarrow \mathbb{R}^d$ med en kartprojektion $x$ är kontinuerlig. Notera att om $\gamma$ är kontinuerlig enligt $x$ är den kontinuerlig även enligt varje annan kartprojektion $y$ med överlappande domän eftersom $y \circ \gamma = y \circ x^{-1} \circ x \circ \gamma$ är kontinuerlig.

\chapter{Multilinjär algebra}

\section{Vektorrum}

Ett \emph{vektorrum} $(V, +, \cdot)$ över en kropp $K$ är en mängd $V$ tillsammans med operationer $+ : V \times V \rightarrow V$ och $\cdot : K \times V \rightarrow V$ (typiskt skrivet utan multiplikationstecknet) som för alla $u,v,w \in V$ och alla $\lambda,\mu \in K$ uppfyller följande:
\begin{enumerate}
\item Kommutativ addition: $u + v = v + u$
\item Associativ addition: $(u + v) + w = u + (v + w)$
\item Existens av ett additivt identitetselement $0$: $u + 0 = u$
\item Existens, för varje $u$, av en additiv invers $-u$: $u + (-u) = 0$
\item Kompatibilitet med multiplikation i $K$: $\lambda (\mu u) = (\lambda \mu) u$
\item Multiplikation är distributiv över vektoraddition: $\lambda(u + v) = \lambda u + \lambda v$
\item Multiplikation är distributiv över skaläraddition: $(\lambda + \mu) u = \lambda u + \mu u$
\item Existens av multiplikativt identitetselement $1$: $1u = u$
\end{enumerate}

\section{Linjär funktion}

Låt $V$ och $W$ vara vektorrum över en gemensam kropp $K$. En funktion $\varphi : V \rightarrow W$ kallas \emph{linjär} om, för alla $u,v \in V$ och alla $\lambda \in K$, följande gäller:
\begin{enumerate}
\item $\varphi(u + v) = \varphi(u) + \varphi(v)$
\item $\varphi(\lambda u) = \lambda \varphi(u)$
\end{enumerate}
En våg betecknar att en funktion är linjär, $\varphi : V \xrightarrow{\sim} W$, och $\operatorname{Hom}(V, W) = \{\varphi : V \xrightarrow{\sim} W\}$ är mängden av alla linjära funktioner från $V$ till $W$. Notera att sammansättningen av två linjära funktioner är linjär.

En funktion som tager flera argument kallas \emph{multilinjär} om den, betraktad som funktion av varje enskilt argument, med övriga argument hållna konstanta, utgör en linjär funktion av detta argument. Vi använder en våg för att beteckna även dessa funktioner.

\section{Linjärt oberoende}

En ändlig mängd element $\{u_0, \ldots, u_{n-1}\} \subseteq V$ ur ett vektorrum $V$ över en kropp $K$ kallas \emph{linjärt oberoende} om det enda sättet att skriva nollvektorn som en linjärkombination av dessa är att sätta alla koefficienter $a^0, \ldots, a^{n-1} \in K$ till noll:
\begin{gather}
  a^0 v_0 + \cdots + a^{n-1} v_{n-1} = 0 \Rightarrow a^0 = \cdots = a^{n-1} = 0
\end{gather}

En oändlig mängd element ur ett vektorrum kallas \emph{linjärt oberoende} om varje ändlig delmängd är linjärt oberoende.

\section{Bas}

Låt $V$ vara ett vektorrum över antingen $\mathbb{R}$ eller $\mathbb{C}$. En delmängd $B \subseteq V$ är en \emph{bas} (Hamel-bas) till $V$ om $B$ är linjärt oberoende och varje element i $V$ kan uttryckas som en ändlig linjärkombination av element i $B$. 

Varje bas till $V$ har samma kardinalitet, så vi kan definiera vektorrummets \emph{dimension}  som $|B|$.

I ett vektorrum $V$ med ändlig bas $B = \{e_0, \ldots, e_{n-1}\}$ kan vi unikt tillskriva en vektor $v \in V$ \emph{komponenter} $v^0, \ldots, v^{n-1}$, med avseende på basen $B$, sådana att
\begin{gather}
  v = v^0 e_0 + \cdots + v^{n-1} e_{n-1}.
\end{gather}

\section{Dualrum}

Låt $V$ vara ett vektorrum över $K$. Dess \emph{dualrum} är $V^* = \{\varphi : V \xrightarrow{\sim} K\}$. Ett element i dualrummet kallas \emph{kovektor}.

Låt $e_0, \ldots, e_{n-1}$ vara en bas för $V$. Den \emph{duala basen} för $V^*$ är den unika bas $\epsilon^0, \ldots, \epsilon^{n-1}$ som uppfyller $\epsilon^a(e_b) = \delta_b^a$.

\section{Tensor}

Låt $V$ vara ett vektorrum över $K$ och låt $r, s \in \mathbb{N}$. En $(r, s)$-tensor $T$ är en multilinjär, skalärvärd funktion av $r$ element ur dualrummet $V^*$ och $s$ element ur $V$:
\begin{gather}
  T : \underbrace{V^* \times \cdots \times V^*}_r \times \underbrace{V \times \cdots \times V}_s \xrightarrow{\sim} K
\end{gather}

Med avseende på en bas $e_0, \ldots, e_{d-1}$ för ett ändligtdimensionellt $V$, och den duala basen $\epsilon^0, \ldots, \epsilon^{d-1}$ kan vi definiera tensorns $(\operatorname{dim} V)^{r+s}$ stycken \emph{komponenter}
\begin{gather}
  T ^{i_0, \ldots, i_{r-1}} {} _{j_0, \ldots, j_{s-1}}
  = T(\epsilon^{i_0}, \ldots, \epsilon^{i_{r-1}}, e_{j_0}, \ldots, e_{j_{s-1}}),
\end{gather}
där varje index $i_k,j_\ell$, $k \in \{0, \ldots r-1\}$, $\ell \in \{0, \ldots s-1\}$, tager värden i $\{0, \ldots, d-1\}$. Tensorn kan återskapas ur sina komponenter:
\begin{multline}
  T(\varphi_0, \ldots, \varphi_{r-1}, v_0, \ldots, v_{s-1})\\
  \begin{split}
    &= T\left(\sum_{i_0=0}^{d-1} (\varphi_0)_{i_0} \epsilon^{i_0},
    \ldots,
    \sum_{i_{r-1}=0}^{d-1} (\varphi_{r-1})_{i_{r-1}} \epsilon^{i_{r-1}},
    \sum_{j_0=0}^{d-1} (v_0)^{j_0} e_{j_0},
    \ldots,
    \sum_{j_{s-1}=0}^{d-1} (v_0)^{j_{s-1}} e_{j_{s-1}}
    \right)\\
    &= \sum_{i_0=0}^{d-1} \cdots \sum_{i_{r-1}=0}^{d-1} \sum_{j_0=0}^{d-1} \cdots \sum_{j_{s-1}=0}^{d-1}
    (\varphi_0)_{i_0} \cdots (\varphi_{r-1})_{i_{r-1}} (v_0)^{j_0} \cdots (v_0)^{j_{s-1}}
    T\left(\epsilon^{i_0}, \ldots, \epsilon^{i_{r-1}}, e_{j_0}, \ldots, e_{j_{s-1}}\right)\\
    &= \sum_{i_0=0}^{d-1} \cdots \sum_{i_{r-1}=0}^{d-1} \sum_{j_0=0}^{d-1} \cdots \sum_{j_{s-1}=0}^{d-1}
    (\varphi_0)_{i_0} \cdots (\varphi_{r-1})_{i_{r-1}} (v_0)^{j_0} \cdots (v_0)^{j_{s-1}}
    T ^{i_0, \ldots, i_{r-1}} {} _{j_0, \ldots, j_{s-1}}\\
    &= (\varphi_0)_{i_0} \cdots (\varphi_{r-1})_{i_{r-1}} (v_0)^{j_0} \cdots (v_0)^{j_{s-1}}
    T ^{i_0, \ldots, i_{r-1}} {} _{j_0, \ldots, j_{s-1}}
  \end{split}
\end{multline}

\chapter{Differentierbara mångfalder}

\section{Kompatibla koordinatbytesfunktioner}

En atlas kallas \emph{$C^k$-kompatibel} om alla dess koordinatbytesfunktioner $x^{-1} \circ y$ ligger i $C^k$, det vill säga är $k$ gånger kontinuerligt differentierbara. På samma sätt kallas en atlas \emph{$C^\infty$-kompatibel} eller \emph{glatt} om alla dess koordinatbytesfunktioner ligger i $C^\infty$.

Varje $C^k$-atlas, för $k \geq 1$, innehåller en $C^\infty$-atlas. (Detta påstående behöver nog modifieras. Kanske menas att varje atlas som innehåller \emph{alla} $C^k$-kartor innehåller en $C^\infty$-atlas.) Vi kan alltså betrakta alla kontinuerligt differentierbara mångfalder som glatta.

Med hjälp av kompatibla kartprojektioner kommer vi definiera differentierbarhet för funktioner från, till och mellan mångfalder, genom att se om motsvarande funktioner mellan rummens koordinatrepresentationer är differentierbara.

\section{Differentierbar kurva}

En kurva $\gamma : \mathbb{R} \rightarrow M$, i en differentierbar mångfald $M$, sägs vara \emph{differentierbar} om sammansättningen $x \circ \gamma : \mathbb{R} \rightarrow \mathbb{R}^d$ är differentierbar. En sådan kurva är automatiskt differentierbar även med avseende på varje annan kartprojektion $y$ med överlappande domän, eftersom $y \circ x^{-1}$ är differentierbar.

\section{Differentierbar funktion}

Med \emph{differentierbar funktion} på en differentierbar mångfald $M$ menas en funktion $f : M \rightarrow \mathbb{R}$ sådan att sammansättningen $x \circ f$ är differentierbar för varje kartprojektion $x$.

\section{Diffeomorfi}

En \emph{glatt avbildning} mellan en glatt $d$-dimensionell mångfald $M$ och en glatt $e$-dimensionell mångfald $N$ är en bijektion $\phi : M \rightarrow N$ sådan att sammansättningen $y \circ \phi \circ x^{-1} : \mathbb{R}^d \rightarrow \mathbb{R}^e$ är glatt för alla kartprojektioner $x$ hos $M$ och $y$ hos $N$. En sådan avbildning kallas \emph{diffeomorfi}.

\chapter{Tangentrum}

\section{Hastighet}

Låt $M$ vara en glatt mångfald och låt $\gamma : \mathbb{R} \rightarrow M$ vara kurva som är $C^1$ och som för något parametervärde $\lambda_0$ träffar punkten $p \in M$, det vill säga $\gamma(\lambda_0) = p$. Vi definierar kurvans \emph{hastighet} i $p$ som den linjära avbildningen $v_{\gamma, p} : C^\infty(M) \xrightarrow{\sim} \mathbb{R}$, $v_{\gamma, p}(f) = (f \circ \gamma)'(\lambda_0)$, där $C^\infty(M)$ vektorrummet av glatta avbildningar $M \rightarrow \mathbb{R}$.

\section{Tangentrum}

Vi definierar \emph{tangentrummet} $T_pM = \{v_{\gamma, p} | \gamma\;\text{glatt}\}$ till mångfalden $M$ i punkten $p$ som mängden av alla hastigheter för glatta kurvor genom $p$. Detta utgör ett vektorrum med operationerna $\oplus : T_pM \times T_pM \rightarrow T_pM$ och $\odot : \mathbb{R} \times T_pM \rightarrow T_pM$:
\begin{gather}
  (v_{\gamma, p} \oplus v_{\delta, p})(f) = v_{\gamma, p}(f) + v_{\delta, p}(f)\\
  (\lambda \odot v_{\gamma, p})(f) = \lambda v_{\gamma, p}(f)
\end{gather}
Det existerar alltid kurvor $\sigma$ och $\tau$ sådana att $v_{\gamma, p} \oplus v_{\delta, p} = v_{\sigma, p}$ och $\lambda \odot v_{\gamma, p} = v_{\tau, p}$, vilket medför att $\oplus$ och $\odot$ verkligen är slutna operationer, det vill säga tager värden i $T_pM$.

\section{Kotangentrum}

Vektorrummet $T_pM$ har ett motsvarande dualrum $T_p^*M = \{\varphi : T_pM \xrightarrow{\sim} \mathbb{R}\}$. Ett typiskt element i detta rum är \emph{gradienten} $(\operatorname{d}f)_p : T_pM \xrightarrow{\sim} \mathbb{R}$ av en funktion $f \in C^\infty(M)$, definierad enligt
\begin{gather}
  (\operatorname{d}f)_p (X) = Xf.
\end{gather}

\section{Kartinducerad bas}
\subsection{Tangentrummet}

En hastighet $v_{\gamma, p}$ kan utvecklas i komponenter genom att applicera den på en glatt funktion $f$ och använda en kartprojektion $x$:
\begin{multline}
  v_{\gamma, p}(f) = (f \circ \gamma)'(\lambda_0)
  =
  \left(\left(f \circ x^{-1}\right) \circ (x \circ \gamma)\right)'(\lambda_0)\\
  =
  ((x \circ \gamma)')^i(\lambda_0)
  \cdot
  ((f \circ x^{-1})')_i (x(p))\\
  =
  \underbrace{((x \circ \gamma)')^i(\lambda_0)}
  _{\dot{\gamma}_x^i(\lambda_0)}
  \cdot
  \underbrace{\left(\partial_i \left(f \circ x^{-1}\right)\right)(x(p))}
  _{\left(\frac{\partial f}{\partial x^i}\right)_p}\\
  =
  \dot{\gamma}_x^i(\lambda_0) \cdot \left(\frac{\partial}{\partial x^i}\right)_p f
\end{multline}
Operatorerna
\begin{gather}
  \left(\frac{\partial}{\partial x^i}\right)_p : C^\infty(M) \xrightarrow{\sim} \mathbb{R}
\end{gather}
utgör basvektorer till tangentrummet $T_pM$ och $\dot{\gamma}_x^i(\lambda_0)$ är de komponenter som i denna bas beskriver hastigheten $v_{\gamma, p}$.

\subsection{Kotangentrummet}

Den duala basen till $T_p^*M$ utgörs av gradienterna $(\operatorname{d} x^i)_p$ av koordinatfunktionerna $x^i$:
\begin{gather}
  (\operatorname{d} x^i)_p \left(\left(\frac{\partial}{\partial x^j}\right)_p\right)
  =
  \left(\frac{\partial x^i}{\partial x^j}\right)_p
  =
  \delta_j^i
\end{gather}
Komponenter, i den duala basen, av en allmän gradient $(\operatorname{d}f)_p$, fås enklast genom att betrakta gradienten som en $(0, 1)$-tensor, vilken alltså tager en tangentvektor som argument, och applicera den på en basvektor till tangentrummet:
\begin{gather}
  \left((\operatorname{d}f)_p\right)_j
  =
  (\operatorname{d}f)_p \left(\left(\frac{\partial}{\partial x^j}\right)_p\right)
  =
  \left(\frac{\partial f}{\partial x^j}\right)_p
\end{gather}

\section{Basbyte}
\subsection{Tangentrummet}

Först skriver vi om en basvektor $(\partial/\partial x^i)_p$ i termer av basvektorer $(\partial/\partial y^j)_p$ ur en annan bas:
\begin{multline}
  \left(\frac{\partial}{\partial x^i}\right)_p f
  =
  \partial_i \left(f \circ x^{-1}\right)(x(p))
  =
  \partial_i \left(\left(f \circ y^{-1}\right) \circ \left(y \circ x^{-1}\right)\right)(x(p))\\
  =
  \underbrace{\partial_i \left(y^j \circ x^{-1}\right) (x(p))}
  _{\left(\frac{\partial y^j}{\partial x^i}\right)_p}
  \cdot
  \underbrace{\partial_j \left(f \circ y^{-1}\right) (y(p))}
  _{\left(\frac{\partial f}{\partial y^j}\right)_p}
\end{multline}
\begin{gather}
  \left(\frac{\partial}{\partial x^i}\right)_p
  =
  \left(\frac{\partial y^j}{\partial x^i}\right)_p
  \cdot
  \left(\frac{\partial}{\partial y^j}\right)_p
\end{gather}
En allmän tangentvektor $X$ får då följande samband mellan sina komponenter $X_{(x)}^i$ i $x$-basen och sina komponenter $X_{(y)}^j$ i $y$-basen:
\begin{gather}
  X
  =
  X_{(x)}^i \left(\frac{\partial}{\partial x^i}\right)_p
  =
  X_{(x)}^i
  \left(\frac{\partial y^j}{\partial x^i}\right)_p
  \left(\frac{\partial}{\partial y^j}\right)_p\\
  X_{(y)}^j
  =
  X_{(x)}^i
  \left(\frac{\partial y^j}{\partial x^i}\right)_p
\end{gather}

\subsection{Kotangentrummet}

Först skriver vi om en basvektor $(\operatorname{d}x^i)_p$ i termer av basvektorer $(\operatorname{d}y^j)_p$ ur en annan bas:
\begin{multline}
  (\operatorname{d}x^i)_p(X)
  =
  Xx^i
  =
  X_{(x)}^j\left(\frac{\partial}{\partial x^j}\right)_p x^i
  =
  X_{(x)}^i
  =
  \left(\frac{\partial x^i}{\partial y^j}\right)_p X_{(y)}^j\\
  =
  \left(\frac{\partial x^i}{\partial y^j}\right)_p (\operatorname{d}y^j)_p(X)
\end{multline}
\begin{gather}
  (\operatorname{d}x^i)_p = \left(\frac{\partial x^i}{\partial y_j}\right)(\operatorname{d}y^j)_p
\end{gather}

En allmän kotangentvektor $\omega$ får då följande samband mellan sina komponenter $\left(\omega_{(x)}\right)_i$ i den duala $x$-basen och sina komponenter $\left(\omega_{(y)}\right)_j$ i den duala $y$-basen:
\begin{gather}
  \omega
  =
  \left(\omega_{(x)}\right)_i (\operatorname{d}x^i)_p
  =
  \left(\omega_{(x)}\right)_i \left(\frac{\partial x^i}{\partial y^j}\right)_p (\operatorname{d}y^j)_p\\
  \left(\omega_{(y)}\right)_j
  =
  \left(\omega_{(x)}\right)_i \left(\frac{\partial x^i}{\partial y^j}\right)_p
\end{gather}

\chapter{Fält}
\section{Knippe}

Låt $M$ och $E$ vara glatta mångfalder.
Ett \emph{glatt knippe} består av ett \emph{basrum} $M$, ett \emph{totalrum} $E$ och en glatt funktion $\pi:E\rightarrow M$, kallad \emph{projektion}.
En \emph{fiber} till en punkt $p\in M$ är tillbakalyftningen $\operatorname{preim}_\pi(\{p\})$.
Ett \emph{snitt} är en funktion $\sigma:M\rightarrow E$, som till varje punkt i $M$ tilldelar en punkt i motsvarande fiber: $\pi\circ\sigma=\operatorname{id}_M$.

\section{Tangentknippe}

Låt $(M,\mathcal{O},\mathcal{A})$ vara en glatt, $d$-dimensionell mångfald.
Vi vill nu skapa ett glatt knippe $TM\xrightarrow{\pi}M$, kallat \emph{tangentknippe}, baserat på denna mångfald och dess tangentrum.

Bilda först
\begin{gather}
  TM = \mathop{\dot{\bigcup}}_{p\in M} T_pM
\end{gather}
som den disjunkta unionen av mångfaldens alla tangentrum.
Bilda därefter $\pi:TM\rightarrow M$ på så sätt att $\pi(X)$ är den unika punkt $p$ vars tangentrum innehåller $X$ ($X\in T_pM$).

Totalrummet $TM$ ska vara en glatt mångfald och behöver därför till att börja med en topologi $\mathcal{O}_{TM}$.
Vi väljer $\mathcal{O}_{TM}$ som den grövsta topologi som gör $\pi$ kontinuerlig:
\begin{gather}
  \mathcal{O}_{TM} = \{\operatorname{preim}_\pi(U) | U\in\mathcal{O}\}
\end{gather}
Därnäst behöver $TM$ en glatt atlas $\mathcal{A}_{TM}$.
Vi utgår från en karta $(U,x)\in\mathcal{A}$ och bildar en motsvarande karta $(TU,\xi_x)\in\mathcal{A}_{TM}$, där domänen $TU=\mathop{\dot{\bigcup}}_{p\in U}T_pM$ är tangentrummen till punkterna i $U$ och där kartprojektionen $\xi_x:TU\rightarrow\mathbb{R}^{2d}$ i sina $d$ första element producerar samma koordinater som $x$ och med sina återstående $d$ koordinater beskriver en tangentvektor i form av dennas komponenter $X_{(x)}^i = (\operatorname{d}x^i)_{\pi(X)}(X)$ i $x$-basen:
\begin{multline}
  \xi_x(X)
  =
  \left(
  \underbrace{\left(x^0\circ\pi\right)(X),\dots,\left(x^{d-1}\circ\pi\right)(X)}
  _{\text{$d$ element}},\right.\\
  \left. \underbrace{(\operatorname{d}x^0)_{\pi(X)}(X),\dots,(\operatorname{d}x^{d-1})_{\pi(X)}(X)}
  _{\text{$d$ element}}
  \right),
\end{multline}
\begin{gather}
  \xi_x^{-1}(\alpha^0,\dots,\alpha^{d-1},\beta^0,\dots,\beta^{-1})
  =
  \beta^i\left(\frac{\partial}{\partial x^i}\right)_{x^{-1}(\alpha^0,\dots,\alpha^{d-1})}.
\end{gather}
Koordinatbytesfunktionerna $\xi_y\circ\xi_x^{-1}$ är glatta och därmed är $(TM,\mathcal{O}_{TM},\mathcal{A}_{TM})$ en glatt mångfald, som önskat.

\section{Kotangentknippe}

Analogt med tangentknippet skapar vi ett glatt \emph{kotangentknippe} $T^*M\xrightarrow{\pi^*}M$, baserat på samma glatta, $d$-dimensionella mångfald $(\mathcal{M},\mathcal{O},\mathcal{A})$:
\begin{gather}
  T^*M = \mathop{\dot{\bigcup}}_{p\in M} T^*_pM\\
  \pi^*(\omega) = p, \quad \omega\in T^*_pM\\
  \mathcal{O}_{T^*M} = \{\operatorname{preim}_{\pi^*}(U)|U\in\mathcal{O}\}
\end{gather}
\begin{multline}
  \xi^*_x(\omega)
  =
  \left(
  \underbrace{\left(x^0\circ\pi\right)(\omega),\dots,\left(x^{d-1}\circ\pi\right)(\omega)}
  _{\text{$d$ element}},\right.\\
  \left. \underbrace{\omega\left(\left(\frac{\partial}{\partial x^0}\right)_{\pi(\omega)}\right),\dots,\omega\left(\left(\frac{\partial}{\partial x^{d-1}}\right)_{\pi(\omega)}\right)}
  _{\text{$d$ element}}
  \right)
\end{multline}
\begin{gather}
  \xi_x^{-1}(\alpha^0,\dots,\alpha^{d-1},\gamma_0,\dots,\gamma_{d-1})
  =
  \gamma_0\left(\operatorname{d}x^0\right)_{x^{-1}(\alpha^0,\dots,\alpha^{d-1})}\\
  \mathcal{A}_{T^*M} = \{(T^*U,\xi^*_x)|(U,x)\in\mathcal{A}\}, \quad T^*U = \mathop{\dot\bigcup}_{p\in U} T^*_pU
\end{gather}

\section{Vektorfält}

Vi återanvänder från och med nu beteckningen $X$, som hittills har representerat en tangentvektor, till att stå för ett vektorfält.

Ett \emph{glatt vektorfält} $X$ på en glatt mångfald $M$ är ett glatt snitt till tangentknippet $TM\xrightarrow{\pi}M$, det vill säga $X:M\rightarrow TM$, $\pi\circ X=\operatorname{id}_M$.
Basvektorerna $(\partial/\partial x^i)_p$, $p\in U$, inom en karta $(U,x)$, kan nu expanderas till motsvarande vektorfält $\partial/\partial x^i$ på hela domänen $U$ enligt $(\partial/\partial x^i)(p) = (\partial/\partial x^i)_p$.
Utöver ett verktorfälts verkan på en punkt $p\in M$ i basrummet, vilket ger en tangentvektor som i sin tur kan verka på en funktion $f\in C^\infty(M)$, definierar vi vektorfältets verkan direkt på $f$ som $X f:M\rightarrow \mathbb{R}$, $(Xf)(p)=X(p)f$.

\section{Kovektorfält}

Likaledes är ett \emph{glatt kovektorfält} ett glatt snitt till kotangentknippet.
En gradient $(\operatorname{d}f)_p$ kan expanderas till ett kovektorfält $\operatorname{d}f$ enligt $(\operatorname{d}f)(p) = (\operatorname{d}f)_p$ och vi definierar även dess verkan på ett vektorfält $X$ som $(\operatorname{d}f)(X)=Xf$. % Här finns en asymmetri mellan hur vektorer respektive kovektorer expanderas. Illa!

\section{Tensorfält}
%\section{Modulen $\Gamma(TM)$ över $C^\infty(M)$}

Funktionerna $C^\infty(M)$, med punktvis addition och punktvis multiplikation, utgör en ring.
Mängden $\Gamma(TM)$ av glatta vektorfält $M\rightarrow TM$ (och, på motsvarande sätt, mängden $\Gamma(T^*M)$ av glatta kovektorfält $M\rightarrow T^*M$) kan göras till en modul över denna ring genom att man definiera additionen $\oplus : \Gamma(TM)\times\Gamma(TM)\rightarrow\Gamma(TM)$ och multiplikationen $\odot : C^\infty(M)\times\Gamma(TM)\rightarrow\Gamma(TM)$.
Låt $X,\tilde{X}\in\Gamma(TM)$, låt $f,g\in C^\infty(M)$ och låt $p\in M$.
Modulen gives nu additionen $(X\oplus\tilde{X})(f)=X f + \tilde{X}f$ och multiplikationen $(g\odot X)(f)=g\cdot X(f)$.

Ett \emph{$(r,s)$-tensorfält} är en $C^\infty(M)$-multilinjär funktion av $r$ kovektorfält och $s$ vektorfält:
\begin{gather}
  T : \underbrace{\Gamma(T^*M) \times \cdots \times \Gamma(T^*M)}_r \times \underbrace{\Gamma(TM) \times \cdots \times \Gamma(TM)}_s \xrightarrow{\sim} C^\infty(M)
\end{gather}

\chapter{Förbindelser}

En \emph{föbindelse} $\nabla$ är en funktion som tager ett vektorfält $X$ och ett $(r,s)$-tensorfält $T$ och avbildar dessa på ett $(r,s)$-tensorfält betecknat $\nabla_X T$, vilket ska betraktas som derivatan av $T$ i riktningen $X$ och vars syfte är att göra tensorer i två näraliggande tangentrum kommensurabla.
Låt $M$ vara en glatt mångfald, låt $X,Y_0,\ldots,Y_{s-1},Z$ vara vektorfält, låt $\omega_0,\ldots,\omega_{r-1}$ vara kovektorfält, låt $f,g\in C^\infty(M)$ och låt $T$ och $S$ vara $(r,s)$-tensorfält.
En förbindelse måste uppfylla följande villkor:
\begin{enumerate}
\item Utvidgning av vektorverkan: $\nabla_X f = Xf$
\item Additivitet i differentianden: $\nabla_X(T+S) = \nabla_X T + \nabla_X S$
\item Leibniz regel: $\nabla_X (T(\omega_0,\ldots,\omega_{r-1}, Y_0,\ldots,Y_{s-1}))
  = (\nabla_X T)(\omega_0,\ldots,\omega_{r-1}, Y_0,\ldots,Y_{s-1})
  + T(\nabla_X \omega_0,\ldots,\omega_{r-1}, Y_0,\ldots,Y_{s-1})
  + \cdots
  + T(\omega_0,\ldots,\nabla_X \omega_{r-1}, Y_0,\ldots,Y_{s-1})
  + T(\omega_0,\ldots,\omega_{r-1}, \nabla_X Y_0,\ldots,Y_{s-1})
  + \cdots
  + T(\omega_0,\ldots,\omega_{r-1}, Y_0,\ldots,\nabla_X Y_{s-1})$ % Detta ser gräsligt ut.
\item Linjäritet i riktningen: $\nabla_{gX+Z} T = g\nabla_X T + \nabla_Z T$
\end{enumerate}

\end{document}
